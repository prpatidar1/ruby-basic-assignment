1) Write a program that prints from 1 to 100.

2) Write a program to check if a value exists in an array.

3) Join two arrays without using inbuilt functions.

4) Write a method to double all the elements in an array.

5) Now for the previous question, write another method to double all the elements in the array. However, handle edge cases (like array can have a character) as well.

6) Using a hash table, print the frequency of occurrence of each character inside an array.

7) Read from a CSV file, multiply two columns and then write back to the CSV file.

8) Write a program to transpose a N*N matrix.
